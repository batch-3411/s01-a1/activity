# 1.
name = "Richener"
age = 26
work = "freelancer"
movie = "John Wick"
rating = float(99.6)

print(f"I am {name}, and I am {age} years old, I work as a {work}, and my rating for {movie} is {rating}")

# 2. 
num1 = 8
num2 = 6
num3 = 10

product = num1 * num2
check = num1 < num3
add = num3 + num2
print(product)
print(check)